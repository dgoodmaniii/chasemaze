/*
 * +AMDG
 */
/*
 * This program was begun on 1X September 1206, the feast of
 * St. Thomas of Villanova, and it is humbly dedicated to
 * him and to the Immaculate Heart of Mary, for their
 * prayers; and to the Sacred Heart of Jesus, for His mercy.
 */

#define _XOPEN_SOURCE 700

#include<stdio.h>
#include<termios.h>
#include"screen_funcs.h"
#include"constants.h"
#include"location.h"

int clean_term(struct termios original, int status)
{
	original.c_lflag |= ICANON;
	original.c_lflag |= ECHO;
	original.c_cc[VMIN]=1;
	original.c_cc[VTIME]=0;
	if(tcsetattr(fileno(stdin), TCSANOW, &original) < 0) {
		perror("Error setting terminal information");
		return -1;
	}
	gotopt(0,HEIGHT);
	closescr(&original);
	return status;
}
