/*
 * +AMDG
 */
/*
 * This documeent was begun on 7 September 1206, and it is
 * humbly dedicated to the Immaculate Heart of Mary, for
 * her prayers, and to the Sacred Heart of Jesus, for His
 * mercy.
 */

#define _POSIX_SOURCE

#include<stdio.h>
#include<stdlib.h>
#include<termios.h>
#include<errno.h>

int initscr(struct termios *original)
{
	struct termios term;
	
	if(tcgetattr(fileno(stdin), &term) < 0) {
		perror("Error getting terminal information");
		return -1;
	}
	*original = term;
	term.c_lflag &= ~ICANON;
	term.c_lflag &= ~ECHO;
	term.c_cc[VMIN]=1;
	term.c_cc[VTIME]=0;
	if(tcsetattr(fileno(stdin), TCSANOW, &term) < 0) {
		perror("Error setting terminal information");
		return -1;
	}
	printf("\033[?25l"); /* hide the cursor */
	return 0;
}

int closescr(struct termios *original)
{
	struct termios term;
	
	if(tcgetattr(fileno(stdin), &term) < 0) {
		perror("Error getting terminal information");
		return -1;
	}
	*original = term;
	term.c_lflag &= ICANON;
	term.c_lflag &= ECHO;
	if(tcsetattr(fileno(stdin), TCSANOW, original) < 0) {
		perror("Error setting terminal information");
		return -1;
	}
	printf("\033[?25h"); /* re-enable the cursor */
	return 0;
}

void clear(void)
{
	printf("\033[2J");
	fflush(stdout);
}
