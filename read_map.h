/*
 * +AMDG
 */
/*
 * This program was begun on 9 Sept 1206, the commemoration
 * of St. Gorgonius, M, and it is humbly dedicated to him
 * and to the Immaculate Heart of Mary, for their prayers;
 * and to the Sacred Heart of Jesus, for His mercy.
 */

#include"constants.h"
#include"data_structs.h"

int read_mapfile(char *s, struct field board[WIDTH+1][HEIGHT+1],
		struct pos *currpos, struct pos *victory);
int list_mapfiles();
