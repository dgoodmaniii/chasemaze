/* +AMDG */

#include<stdio.h>
#include<termios.h>
#include<signal.h>
#include<unistd.h>
#include<getopt.h>
#include<stdlib.h>
#include<sys/time.h>
#include"screen_funcs.h"
#include"read_map.h"
#include"getch.h"
#include"location.h"
#include"data_structs.h"
#include"constants.h"
#include"popup.h"
#include"chase_game.h"
#include"game_sigs.h"
#include"cleanup.h"
#include"errcodes.h"

struct field board[WIDTH+1][HEIGHT+1];
struct pos currpos = { 2, 2 };
struct pos victory = { WIDTH - 2, HEIGHT - 2};

int main(int argc, char **argv)
{
	struct termios original;
	char c, d, e;
	char gametype = GT_MAZE;
	struct itimerval victtimer;
	int tsecs = 3;
	int tmils = 0;
	opterr = 0;
	int status = 0;
	void (*movefunc)(int x, int y, struct field board[WIDTH+1][HEIGHT+1],struct pos *currpos, int dir) = &move;

	clear();
	initscr(&original);
	init_field(board);
	while ((e = getopt(argc, argv, "Vmcldt:T:f:")) != -1) {
		switch (e) {
		case 'V':
			printf("chasemaze v1.0.\n");
			printf("Copyright (C) 1206 (2022), Donald P.  Goodman III.\n");
			printf("License GPLv3+:  GNU GPL version 3 or later "
					"<https://gnu.org/licenses/gpl.html>\n"
					"This is free software:  you are free to "
					"change and redistribute it.\n"
					"There is NO WARRANTY, to the extent "
					"permitted by law.\n");
			return clean_term(original,SUCCESS);
			break;
		case 'l':
			return clean_term(original,list_mapfiles());
			break;
		case 'm':
			gametype = GT_MAZE;
			break;
		case 'c':
			gametype = GT_CHASE;
			break;
		case 't':
			tsecs = atoi(optarg);
			break;
		case 'T':
			tmils = atoi(optarg) * 1000;
			break;
		case 'd':
			movefunc = &omove;
			break;
		case 'f':
			status = read_mapfile(optarg,board,&currpos,&victory);
			if (status != 0)
				return clean_term(original,status);
			break;
		case '?':
			if (optopt == 't') {
				fprintf(stderr,"chasemaze:  option '-t' requires the "
						"number of seconds in between moving the goal\n");
				exit(ERR_NUMSECS);
			}
			if (optopt == 'T') {
				fprintf(stderr,"chasemaze:  option '-T' requires the "
						"number of tenths of a second in between "
						"moving the goal\n");
				exit(ERR_NUMMILS);
			}
			fprintf(stderr,"chasemaze:  unknown option '%c'\n",optopt);
			exit(ERR_UNKNOWN_OPT);
			break;
		}
	}
	fflush(stdout);
	set_victory(&victory,board);
	while (ifcollide(currpos.x,currpos.y,board) == 1) {
		++(currpos.y);
		if (currpos.y == (HEIGHT-1)) {
			currpos.y = 0;
			++(currpos.x);
		}
		if (currpos.x == (WIDTH-1)) {
			return clean_term(original,ERR_NO_PLAYER_POS);
		}
	}
	if (comp_pos(&currpos,&victory) == 0) {
		fprintf(stderr,"chasemaze:  error:  no free location "
				"for the player to start\n");
		return clean_term(original,ERR_NO_PLAYER_POS);
	} else {
		board[currpos.x][currpos.y].fill = '@';
		board[currpos.x][currpos.y].stat = STATE_PLAY;
	}
	print_field(board);
	if (gametype == GT_MAZE) {
		maze_popup();
		while ((d = getch()) != ' ');
		print_field(board);
	}
	if (gametype == GT_CHASE) {
		chase_popup();
		while ((d = getch()) != ' ');
		print_field(board);
		victtimer.it_value.tv_sec = tsecs;
		victtimer.it_value.tv_usec = tmils;
		victtimer.it_interval.tv_sec = tsecs;
		victtimer.it_interval.tv_usec = tmils;
		setitimer(ITIMER_REAL,&victtimer,NULL);
		signal(SIGALRM,randomize);
	}
	while ((c = getch()) != 'q') {
		signal(SIGALRM,randomize);
		switch (c) {
		case 'k':
			movefunc(currpos.x, currpos.y-1, board, &currpos, UP);
			break;
		case 'j':
			movefunc(currpos.x, currpos.y+1, board, &currpos, DOWN);
			break;
		case 'h':
			movefunc(currpos.x-1, currpos.y, board, &currpos, LEFT);
			break;
		case 'l':
			movefunc(currpos.x+1, currpos.y, board, &currpos, RIGHT);
			break;
		}
		if (comp_pos(&currpos,&victory) == 0) {
			if (gametype == GT_CHASE) {
				victtimer.it_value.tv_sec = 0;
				victtimer.it_value.tv_usec = 0;
				victtimer.it_interval.tv_sec = 0;
				victtimer.it_interval.tv_usec = 0;
				setitimer(ITIMER_REAL,&victtimer,NULL);
				signal(SIGALRM,randomize);
			}
			victory_popup();
			while ((d = getch()) != 'q');
			break;
		}
	}
/*	clear();
	gotopt(0,HEIGHT);
	closescr(&original);*/
	clear();
	return clean_term(original,SUCCESS);
}
