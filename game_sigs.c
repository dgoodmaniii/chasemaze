/*
 * +AMDG
 */
/*
 * This program was begun on 1X September 1206, the feast of
 * St. Thomas of Villanova, and it is humbly dedicated to
 * him and to the Immaculate Heart of Mary, for their
 * prayers; and to the Sacred Heart of Jesus, for His mercy.
 */

#include"location.h"
#include"chase_game.h"

extern struct field board[WIDTH+1][HEIGHT+1];
extern struct pos victory;

void randomize(int signo)
{
	chase_vict(board,&victory);
	print_field(board);
}
