." +AMDG
." Process with:
." groff -man -Tascii chasemaze.1
.TH chasemaze 1 "June 2016" dgoodmaniii chasemaze
.SH NAME
chasemaze \- a terminal-based maze and chase game
.SH SYNOPSIS
.B chasemaze
[\fB\-V\fR]
[\fB\-m\fR]
[\fB\-l\fR]
[\fB\-c\fR]
[\fB\-d\fR]
[\fB\-t\fR \fIseconds\fR]
[\fB\-T\fR \fItenths of a second\fR]
[\fB\-f\fR \fI[mapfile]\fR]
.SH DESCRIPTION
.B chasemaze
A terminal-based game, which allows completing mazes with a
stationary goal-point or completing chases, where the
goal-point moves around the screen.
.SH OPTIONS
.TP
.BR \-V
Print version and licensing information, then exit
successfully.
.TP
.BR \-l
List the available map files and quit successfully.  If
available, a description will be printed.  See the section
on map files for writing a description.
.TP
.BR \-m
Play in maze mode; this is the default, in which the
goalpoint does not move.
.TP
.BR \-c
Play in chase mode, in which the goalpoint does change.  The
default change rate is every 2.5 seconds; that can be
altered with the \fI-t\fR and \fI-T\fR options.
.TP
.BR "\-d"
Difficult mode; this means that once you've occupied a
space, you cannot occupy that space again.  Especially makes
mazes quite difficult.
.TP
.BR "\-t \fIseconds\fR"
Alters the number of seconds that it takes for
\fBchasemaze\fR to change the location of the goalpoint.
.TP
.BR "\-T \fItenths of a second\fR"
Changes the among of time it takes for \fBchasemaze\fR to
change the location of the goalpoint; this argument is
entered in tenths of a second.
.TP
.BR "\-f \fIfile/directory\fR"
Instructs \fBchasemaze\fR to use the named file as its map
file.
.SH GAMEPLAY
.PP
In \fImaze mode\fR, your character (the '@' sign) must get
to the goalpoint (the '*' sign), through whatever obstacles
may be presented by the map file.  By default, the map file
is a completely empty screen, so there are no obstacles to
avoid.
.PP
In \fIchase mode\fR, your character (still the '@' sign)
must get to the goalpoint (the '*' sign), but this time the
goalpoint bounces around the screen at random.  It will move
to another random point every 2.5 seconds by default; you
can change this by specifying a number of seconds, with
\fI-t\fR, and a number of tenths of a second, with \fI-T\fR.
.SH MAP FILES
You can provide any map file you'd like to \fBchasemaze\fR,
subject to the restrictions of format:  (1) The map file
must contain at least 23 lines; and (2) each line of the map
file must contain exactly 80 characters.
.PP
The map file cannot contain less than 23 lines. Less
than 23 lines may be part of the maze, but if your map is less
than 23 lines, you must pad it to 23 with blank lines for
\fBchasemaze\fR to understand it.  Any lines after the
twenty-third will be simply ignored by \fBchasemaze\fR; so
lines after the twenty-third are excellent places for
credits (say, who made the mapfile).
.PP
A description will be printed with the \fI-l\fR option if
one is available.  This description will be taken from the
map file itself; if \fBchasemaze\fR encounters the string
\fIDESC:\fR in the map file, the remainder of the line on
which that string is found will be printed as the
description when \fI-l\fR is specified.  So a line of this
form:
.PP
.RS
DESC:  A really tough (borderline impossible) maze.
.RE
.PP
in a map file called "tough" will be printed, with the
\fI-l\fR option, as such:
.PP
.RS
tough:  A really tough (borderline impossible) maze.
.RE
.PP
This gives your users a bit of information when trying to
choose a map file to play.
.PP
The map file must also contain a location for the player
character to start, which will be the character '@', and a
goalpoint, which will be the character '*'.  If these are
not included, the player character will be placed as close
as possible to (2,2) and the goalpoint as close as possible
to the same place on the bottom right of the screen.  If
there is no place for \fBchasemaze\fR to put either of the
two points, it will complain to you and quit.
.PP
The distribution comes with a few maps, one intended for
chase-mode (\fIchase_one\fR) and a few intended for
maze-mode.  By default, upon installation these will
be saved to \fI~/.config/chasemaze/\fR.  \fBchasemaze\fR
looks first in the current directory; if it cannot find the
map file you've asked for there, it will look in that
directory named above.  If it cannot find it there, either,
it will fail.
.PP
Other than these restrictions, anything goes.
\fBchasemaze\fR makes no attempt to verify that your map is
actually winnable, so make sure that you do so when you're
defining it.  Any ASCII characters can be used to create
tunnels, barriers, or obstacles; the old standbys hyphen,
underscore, forward- and backslash, and pipes do a pretty
good job of this.
.PP
You can use arrows ('^', '<', '>', 'v') if you want, but be
aware that in hard mode \fBchasemaze\fR uses these to
indicate where the player has already been, so that may
create confusion.
.SH ERRORS
\fBchasemaze\fR can, like all programs, go wrong in a huge
number of ways; but fortunately, most of those are extremely
unlikely.  Still, \fBchasemaze\fR tries hard to give precise
failure information, even for those situations that
basically never happen.
.PP
So upon failure, \fBchasemaze\fR prints both an error message
to \fIstderr\fR and exits with a particular status.  Error
codes are as follows:
.TP
.BR 0
Success!  Nothing went wrong (at least, nothing that
\fBchasemaze\fR recognizes).  Hooray!
.TP
.BR 1
You gave \fBchasemaze\fR a command line option it doesn't
recognize.
.TP
.BR 2
You used the \fI-t\fR option, but didn't give a valid number
of seconds for it.
.TP
.BR 3
You used the \fI-T\fR option, but didn't give a valid number
of tenths of a second for it.
.TP
.BR 4
The map file you provided to \fBchasemaze\fR has at least
one line which is too long.
.TP
.BR 5
The map file you provided to \fBchasemaze\fR has at least
one line which is too short.
.TP
.BR 6
The map file you provided to \fBchasemaze\fR is at least one
line too short.  (If it's too long, \fBchasemaze\fR just
ignores all the lines past the twenty-third, so no error
will be reported.)
.TP
.BR 7
The map file you gave \fBchasemaze\fR not only has no player
position specified, but gives \fBchasemaze\fR no place to put
a player, either.
.TP
.BR 8
File error; for some reason, \fBchasemaze\fR was not able to
open the map file you asked it to use.  This is likely
either because the file doesn't exist, or because you don't
have permissions to read it.
.SH BUGS
None known at this time.
.SH AUTHOR
Donald P. Goodman III <dgoodmaniii at gmail dot com>
