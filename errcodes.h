/*
 * +AMDG
 */
/*
 * This program was begun on 12 Sept 1206, the feast of the
 * Exaltation of the Holy Cross, and it is humbly dedicated
 * to Jesus Christ, for His mercy.  We adore thee, O Christ,
 * and we bless Thee, because by Thy holy Cross thou hast
 * redeemed the world.
 */

#define	SUCCESS	0
#define	ERR_UNKNOWN_OPT	1
#define	ERR_NUMSECS	2
#define	ERR_NUMMILS	3
#define	ERR_LINE_LONG	4
#define	ERR_LINE_SHORT	5
#define	ERR_FILE_SHORT	6
#define	ERR_NO_PLAYER_POS	7
#define	FILE_ERR	8
#define	ERR_NO_HOMEDIR	9
#define	ERR_INSUFF_MEM	10
