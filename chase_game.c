/*
 * +AMDG
 */
/*
 * This program was begun on 11 Sept 1206, and it is humbly
 * dedicated to the Immaculate Heart of Mary, for her
 * prayers, and to the Sacred Heart of Jesus, for His mercy.
 */

#include<stdlib.h>
#include<time.h>
#include"constants.h"
#include"data_structs.h"

int chase_vict(struct field board[WIDTH+1][HEIGHT+1],
		struct pos *victory)
{
	int x, y;

	board[victory->x][victory->y].stat = STATE_EMPTY;
	board[victory->x][victory->y].fill = ' ';
	srand(time(NULL));
	x = rand() % WIDTH;
	y = rand() % HEIGHT;
	while ((board[x][y].stat == STATE_OCC) ||
			(board[x][y].stat == STATE_PLAY)) {
		x = rand() % WIDTH;
		y = rand() % HEIGHT;
	}
	victory->x = x;
	victory->y = y;
	board[x][y].stat = STATE_GOAL;
	board[x][y].fill = '*';
	return 0;
}

