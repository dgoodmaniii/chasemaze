/*
 * +AMDG
 */
/*
 * This program was begun on 8 Sept 1206, the commemoration
 * of St. Adrian, M, and it is humbly dedicated to him and
 * to the Immaculate Heart of Mary, for their prayers, and
 * to the Sacred Heart of Jesus, for His mercy.
 */

#ifndef STRUCT_DEF
#define STRUCT_DEF
struct pos {
	int x;
	int y;
};

struct field {
	int x;
	int y;
	int stat;
	char fill;
};
#endif
