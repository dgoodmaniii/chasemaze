/*
 * +AMDG
 */
/*
 * This program was begun on 8 Sept 1206, the commemoration
 * of St. Adrian, M, and it is humbly dedicated to him and
 * to the Immaculate Heart of Mary, for their prayers, and
 * to the Sacred Heart of Jesus, for His mercy.
 */

#include<stdio.h>
#include"data_structs.h"
#include"constants.h"

void init_field(struct field board[WIDTH+1][HEIGHT+1])
{
	int i, j;

	for (i = 0; i < WIDTH; ++i) {
		for (j = 0; j < HEIGHT; ++j) {
			board[i][j].x = i;
			board[i][j].y = j;
			board[i][j].fill = ' ';
			board[i][j].stat = STATE_EMPTY;
		}
	}
}

void gotopt(int x, int y)
{
	printf("\033[%d;%dH",y,x);
	fflush(stdout);
}

/* return 1 if a collision, 0 if not */
int ifcollide(int x, int y, struct field board[WIDTH+1][HEIGHT+1])
{
	if ((board[x][y].stat != STATE_EMPTY) &&
			(board[x][y].stat != STATE_GOAL))
		return 1;
	if ((x == 0) || (y == 0) || (x == WIDTH) || (y == HEIGHT))
		return 1;
	return 0;
}

void move(int x, int y, struct field board[WIDTH+1][HEIGHT+1],
		struct pos *currpos, int dir)
{
	if (ifcollide(x,y,board) != 1) {
		gotopt(currpos->x,currpos->y);
		printf(" ");
		board[currpos->x][currpos->y].stat = STATE_EMPTY;
		board[currpos->x][currpos->y].fill = ' ';
		gotopt(x,y);
		currpos->x = x;
		currpos->y = y;
		board[currpos->x][currpos->y].stat = STATE_PLAY;
		board[currpos->x][currpos->y].fill = '@';
		printf("%c",PLAYER);
		fflush(stdout);
	}
}

void omove(int x, int y, struct field board[WIDTH+1][HEIGHT+1],
		struct pos *currpos, int dir)
{
	if (ifcollide(x,y,board) != 1) {
		board[currpos->x][currpos->y].stat = STATE_OCC;
		gotopt(currpos->x,currpos->y);
		if (dir == UP) {
			board[currpos->x][currpos->y].fill = '^';
			printf("^");
		} else if (dir == LEFT) {
			board[currpos->x][currpos->y].fill = '<';
			printf("<");
		} else if (dir == RIGHT) {
			board[currpos->x][currpos->y].fill = '>';
			printf(">");
		} else {
			board[currpos->x][currpos->y].fill = 'v';
			printf("v");
		}
		gotopt(x,y);
		currpos->x = x;
		currpos->y = y;
		printf("%c",PLAYER);
		fflush(stdout);
	}
}

void print_field(struct field board[WIDTH+1][HEIGHT+1])
{
	int i, j;
	for (i = 0; i < HEIGHT; ++i) {
		for (j = 0; j < WIDTH; ++j) {
			gotopt(j,i);
			printf("%c",board[j][i].fill);
		}
	}
	fflush(stdout);
}

/* return 0 if the same, 1 if not */
int comp_pos(struct pos *posone, struct pos *postwo)
{
	if ((posone->x == postwo->x) && (posone->y == postwo->y))
		return 0;
	return 1;
}

void set_victory(struct pos *victory, 
		struct field board[WIDTH+1][HEIGHT+1])
{
	board[victory->x][victory->y].stat = STATE_GOAL;
	board[victory->x][victory->y].fill = '*';
}
