/*
 * +AMDG
 */
/*
 * This program was begun on 8 Sept 1206, the commemoration
 * of St. Adrian, M, and it is humbly dedicated to him and
 * to the Immaculate Heart of Mary, for their prayers, and
 * to the Sacred Heart of Jesus, for His mercy.
 */

#include"data_structs.h"
#include"constants.h"

void init_field(struct field board[WIDTH+1][HEIGHT+1]);
void gotopt(int x, int y);
void print_field(struct field board[WIDTH+1][HEIGHT+1]);
void move(int x, int y, struct field board[WIDTH+1][HEIGHT+1],
		struct pos *currpos, int dir);
void omove(int x, int y, struct field board[WIDTH+1][HEIGHT+1],
		struct pos *currpos, int dir);
/* return 0 if the same, 1 if not */
int comp_pos(struct pos *posone, struct pos *postwo);
void set_victory(struct pos *victory, 
		struct field board[WIDTH+1][HEIGHT+1]);
int ifcollide(int x, int y, struct field board[WIDTH+1][HEIGHT+1]);
int comp_pos(struct pos *posone, struct pos *postwo);
