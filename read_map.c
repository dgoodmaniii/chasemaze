/*
 * +AMDG
 */
/*
 * This program was begun on 9 Sept 1206, the commemoration
 * of St. Gorgonius, M, and it is humbly dedicated to him
 * and to the Immaculate Heart of Mary, for their prayers;
 * and to the Sacred Heart of Jesus, for His mercy.
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<dirent.h>
#include"constants.h"
#include"data_structs.h"
#include"errcodes.h"

int read_mapfile(char *s, struct field board[WIDTH+1][HEIGHT+1],
		struct pos *currpos, struct pos *victory)
{
	FILE *fp;
	char c;
	int x = 1;
	int y = 1;
	char *defloc = "/.config/chasemaze/";
	char *home;
	char *filetoopen = NULL;

	home = getenv("HOME");
	if ((fp = fopen(s,"r")) == NULL) {
		if ((filetoopen = malloc(strlen(home) + strlen(defloc) +
						strlen(s) + 1)) != NULL) {
			filetoopen[9] = '\0';
			strcat(filetoopen,home);
			strcat(filetoopen,defloc);
			strcat(filetoopen,s);
			filetoopen[strlen(home) + strlen(defloc)+strlen(s)] = '\0';
			if ((fp = fopen(filetoopen,"r")) == NULL) {
				fprintf(stderr,"chasemaze:  error:  cannot open file \"%s\"\n",s);
				fprintf(stderr,"\tError No. %d:  %s\n",errno,strerror(errno));
				return FILE_ERR;
			}
		} else {
			fprintf(stderr,"chasemaze:  error:  insufficient memory\n");
			exit(15);
		}
	}
	while ((c = fgetc(fp)) != EOF) {
		if (c == '\n') {
			if (x < (WIDTH)) {
				fprintf(stderr,"chasemaze:  error:  each line in "
						"a map file must be eighty characters long; "
						"line %d is only %d characters long.  "
						"Aborting.\n",y,x-1);
				return ERR_LINE_SHORT;
			}
		}
		board[x][y].fill = c;
		if (c == '@') {
			currpos->x = x;
			currpos->y = y;
			board[x][y].stat = STATE_EMPTY;
		} else if (c == '*') {
			victory->x = x;
			victory->y = y;
			board[x][y].stat = STATE_GOAL;
		} else if (c != ' ') {
			board[x][y].stat = STATE_OCC;
		} else {
			board[x][y].stat = STATE_EMPTY;
		}
		if (x > (WIDTH-1)) {
			if (c != '\n') {
				fprintf(stderr,"chasemaze:  error:  each line in "
						"a map file must be eighty characters long; "
						"line %d is at least %d characters long.  "
						"Aborting.\n",y,x);
				return ERR_LINE_LONG;
			}
			x = 1;
			++y;
		} else {
			++x;
		}
		if (y == (HEIGHT))
			break;
	}
	if (y != (HEIGHT)) {
		fprintf(stderr,"chasemaze:  error:  a map file "
				"must be exactly %d lines long; "
				"map file \"%s\" is only %d lines long.  "
				"Aborting.\n", HEIGHT-1, s, y-1);
		return ERR_FILE_SHORT;
	}
	if (filetoopen != NULL)
		free(filetoopen);
	return 0;
}

int list_mapfiles()
{
	FILE *fp;
	DIR *dir;
	struct dirent *dp;
	char c;
	int i = 0;
	char *defloc = "/.config/chasemaze/";
	char *home;
	char *homedir = NULL;
	char *currfile = NULL;

	home = getenv("HOME");
	if ((homedir = malloc(strlen(home) + strlen(defloc) + 2)) != NULL) {
		homedir[0] = '\0';
		strcat(homedir,home);
		strcat(homedir,defloc);
		homedir[strlen(home) + strlen(defloc)] = '\0';
		if ((dir = opendir(homedir)) == NULL) {
			fprintf(stderr,"chasemaze:  error:  could not open "
					"default directory \"%s\"\n",homedir);
			free(homedir);
			closedir(dir);
			return ERR_NO_HOMEDIR;
		}
		if ((currfile = malloc(strlen(homedir) + 1)) == NULL) {
			fprintf(stderr,"chasemaze:  error:  insufficient memory\n");
			return ERR_INSUFF_MEM;
		}
		while ((dp = readdir(dir)) != NULL) {
			if ((strcmp(dp->d_name,".") != 0) &&
					(strcmp(dp->d_name,"..") != 0)) {
				if ((currfile = realloc(currfile,(strlen(homedir)
								+ strlen(dp->d_name) + 3))) == NULL) {
					fprintf(stderr,"chasemaze:  error:  insufficient memory\n");
					return ERR_INSUFF_MEM;
				}
				currfile[0] = '\0';
				strcat(currfile,homedir);
				strcat(currfile,"/");
				strcat(currfile,dp->d_name);
				printf("%15s",dp->d_name);
				if ((fp = fopen(currfile,"r")) == NULL) {
					fprintf(stderr,"chasemaze:  error:  could not "
							"open file \"%s\"\n",dp->d_name);
					perror(dp->d_name);
					continue;
				}
				while ((c = fgetc(fp)) != EOF) {
					if (c == 'D') {
						++i;
					} if ((c == 'E') && (i == 1)) {
						++i;
					} if ((c == 'S') && (i == 2)) {
						++i;
					} if ((c == 'C') && (i == 3)) {
						++i;
					} if ((c == ':') && (i == 4)) {
						++i;
					}
					if (i == 5) {
						if (c != '\n')
							printf("%c",c);
					}
				}
				i = 0;
				printf("\n");
			}
		}
	} else {
		fprintf(stderr,"chasemaze:  error:  insufficient memory\n");
		free(homedir);
		closedir(dir);
		return ERR_INSUFF_MEM;
	}	
	free(currfile);
	closedir(dir);
	free(homedir);
	return SUCCESS;
}
