/*
 * +AMDG
 */
/*
 * This document was begun on the feast of the Most Holy
 * Name of Mary, and it is humbly dedicated to her, for her
 * prayers, and to the Sacred Heart of Jesus, for His mercy.
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"constants.h"
#include"location.h"

void popupstr(int x, int y, char *s)
{
	gotopt(x,y);
	printf("%s",s);
	fflush(stdout);
}

void popup(int wd, int ht)
{
	int x; int y;
	int i, j;

	x = (WIDTH / 2) - (wd / 2);
	y = (HEIGHT / 2) - (ht / 2);
	gotopt(x,y);
	printf("+");
	for (i = x+1; i < (x + wd); ++i)
		printf("-");
	printf("+");
	for (j = 1; j < ht; ++j) {
		gotopt(x,y+j);
		printf("|");
		for (i = x+1; i < (x + wd); ++i) 
			printf(" ");
		printf("|");
	}
	gotopt(x,y+j);
	printf("+");
	for (i = x+1; i < x + wd; ++i)
		printf("-");
	printf("+");
	fflush(stdout);
}
			
void victory_popup()
{
	popup(60,8);
	popupstr(14,9,"__      _______ _____ _______ ____  _______     ___ ");
	popupstr(14,10,"\\ \\    / /_   _/ ____|__   __/ __ \\|  __ \\ \\   / / |");
	popupstr(14,11," \\ \\  / /  | || |       | | | |  | | |__) \\ \\_/ /| |");
	popupstr(14,12,"  \\ \\/ /   | || |       | | | |  | |  _  / \\   / | |");
	popupstr(14,13,"   \\  /   _| || |____   | | | |__| | | \\ \\  | |  |_|");
	popupstr(14,14,"    \\/   |_____\\_____|  |_|  \\____/|_|  \\_\\ |_|  (_)");
	popupstr(32,15,"Press 'q' to quit");
}

void maze_popup()
{
	popup(76,20);
	popupstr(4,4,"MMMM              MMMMM                                                   ");
	popupstr(4,5,"M:::M            M::::M                                                   ");
	popupstr(4,6,"M::::M          M:::::M                                                   ");
	popupstr(4,7,"M:::::M        M::::::M                                                   ");
	popupstr(4,8,"M::::::M      M:::::::M aaaaaaaaaaaaa  zzzzzzzzzzzzzzzz  eeeeeeeeeeee    ");
	popupstr(4,9,"M:::::::M    M::::::::M a::::::::::::a z::::::::::::::z e::::::::::::ee  ");
	popupstr(4,10,"M:::M::::M  M::::M::::M aaaaaaaaa:::::az:::::::::::::z e:::::eeeee:::::ee");
	popupstr(4,11,"M:::M M:::MM::::M M:::M          a::::azzzzzzz::::::z e:::::e     e:::::e");
	popupstr(4,12,"M:::M  M:::M:::M  M:::M   aaaaaaa:::::a      z:::::z  e::::::eeeee::::::e");
	popupstr(4,13,"M:::M   M:::::M   M:::M aa::::::::::::a     z:::::z   e::::::::::::::::e ");
	popupstr(4,14,"M:::M    M:::M    M:::Ma::::aaaa::::::a    z:::::z    e:::::eeeeeeeeeee  ");
	popupstr(4,15,"M:::M     MMM     M:::Ma::::a    a::::a   z:::::z     e::::::e          ");
	popupstr(4,16,"M:::M             M:::Ma::::a    a::::a  z:::::zzzzzzze:::::::e");
	popupstr(4,17,"M:::M             M:::Ma:::::aaaa:::::a z::::::::::::z e::::::eeeeeeee");
	popupstr(4,18,"M:::M             M:::Ma::::::aa:::::a z:::::::::::::z  e::::::::::::e  ");
	popupstr(4,19,"MMMMM             MMMMM aaaaaaaaaaaaa zzzzzzzzzzzzzzzz   eeeeeeeeeeee");
	popupstr(30,21,"Press space to play");
}

void chase_popup()
{
	popup(76,20);
	popupstr(20,7,"           __________");
	popupstr(20,8,"          /         /");
	popupstr(20,9,"         /   ______/");
	popupstr(20,10,"        /   /  __");
	popupstr(20,11,"       /   /  / /                 ");
	popupstr(20,12,"      /   /  / /_   ____   _____ ___ ");
	popupstr(20,13,"     /   /  / __ \\ / __ \\ /____// _ \\");
	popupstr(20,14,"    /   /  / / / // / /_/(___ )/  __/");
	popupstr(20,15,"   /   /  /_/ /_/ \\__,_/ /___/ \\___/ ");
	popupstr(20,16,"  /   /_______  ");
	popupstr(20,17," /           / ");
	popupstr(20,18," \\__________/  ");
	popupstr(30,21,"Press space to play");
}
