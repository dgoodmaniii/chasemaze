/*
 * +AMDG
 */
/*
 * This program was begun on 11 Sept 1206, and it is humbly
 * dedicated to the Immaculate Heart of Mary, for her
 * prayers, and to the Sacred Heart of Jesus, for His mercy.
 */

int chase_vict(struct field board[WIDTH+1][HEIGHT+1],
		struct pos *victory);
