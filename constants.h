/*
 * +AMDG
 */
/*
 * This program was begun on 8 Sept 1206, the commemoration
 * of St. Adrian, M, and it is humbly dedicated to him and
 * to the Immaculate Heart of Mary, for their prayers, and
 * to the Sacred Heart of Jesus, for His mercy.
 */

#define	WIDTH	81
#define	HEIGHT	24

#define	PLAYER	'@'

#define	STATE_EMPTY	0
#define	STATE_OCC	1
#define	STATE_PLAY	2
#define	STATE_GOAL	3

#define	RIGHT	0
#define	LEFT	1
#define	UP	2
#define	DOWN	3

#define	GT_MAZE	0
#define	GT_CHASE	1
