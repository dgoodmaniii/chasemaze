# +AMDG
# This makefile was begun on 7 Sept 1206, and it is humbly
# dedicated to the Immaculate Heart of Mary, for her
# prayers, and to the Sacred Heart of Jesus, for His mercy.

.POSIX:

CC=gcc
CFLAGS=-Wall -g -std=c99 -pedantic
OBJ=screen_funcs.o getch.o location.o read_map.o popup.o \
	chase_game.o game_sigs.o cleanup.o
LIBS=constants.h data_structs.h getch.h location.h screen_funcs.h \
	  read_map.h popup.h chase_game.h game_sigs.h cleanup.h
MAPS=chase_one maze_one Maze Maze_two Maze_three
PREFIX=/usr/local
BINFILE=chasemaze
MANFILE=chasemaze.1

all: chasemaze

chasemaze: main.c $(OBJ) $(LIBS)
	$(CC) $(CFLAGS) -o chasemaze main.c $(OBJ)

cleanup.o: cleanup.c cleanup.h
	$(CC) $(CFLAGS) -c cleanup.c

game_sigs.o:  game_sigs.c data_structs.h
	$(CC) $(CFLAGS) -c game_sigs.c

chase_game.o: chase_game.c chase_game.h
	$(CC) $(CFLAGS) -c chase_game.c

popup.o: popup.c popup.h
	$(CC) $(CFLAGS) -c popup.c

read_map.o: read_map.h read_map.c constants.h data_structs.h errcodes.h
	$(CC) $(CFLAGS) -c read_map.c

location.o: location.c location.h data_structs.h constants.h
	$(CC) $(CFLAGS) -c location.c

screen_funcs.o: screen_funcs.c screen_funcs.h
	$(CC) $(CFLAGS) -c screen_funcs.c

getch.o: getch.c getch.h
	$(CC) $(CFLAGS) -c getch.c

.PHONY: install
install:
	mkdir -p $(HOME)/.config/chasemaze
	cp -f $(MAPS) $(HOME)/.config/chasemaze/
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(PREFIX)/share/man/man1
	cp -f $(BINFILE) $(DESTDIR)$(PREFIX)/bin
	cp -f $(MANFILE) $(DESTDIR)$(PREFIX)/share/man/man1/

.PHONY: uninstall
uninstall :
	rm $(DESTDIR)$(PREFIX)/bin/$(BINFILE)
	rm $(DESTDIR)$(PREFIX)/share/man/man1/$(MANFILE)
	rm -Rf $(HOME)/.config/chasemaze/

.PHONY: clean
clean:
	-rm $(OBJ) $(BINFILE)
