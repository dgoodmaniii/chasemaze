/*
 * +AMDG
 */
/*
 * This program was begun on 1X September 1206, the feast of
 * St. Thomas of Villanova, and it is humbly dedicated to
 * him and to the Immaculate Heart of Mary, for their
 * prayers; and to the Sacred Heart of Jesus, for His mercy.
 */

int clean_term(struct termios original, int status);
