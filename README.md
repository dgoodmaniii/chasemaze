NOTE:  I have recently become aware that integer overflows
can cause security issues.  I don't think there are likely
to be such problems here, but if there are, I don't really
know how to find them or fix them.  So, as always, use at
your own risk.
