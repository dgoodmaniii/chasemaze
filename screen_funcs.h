/*
 * +AMDG
 */
/*
 * This documeent was begun on 7 September 1206, and it is
 * humbly dedicated to the Immaculate Heart of Mary, for
 * her prayers, and to the Sacred Heart of Jesus, for His
 * mercy.
 */

int initscr(struct termios *original);
int closescr(struct termios *original);
void clear(void);
